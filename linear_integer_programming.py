from datetime import datetime
from fractions import Fraction
import numpy as np
from math import ceil, floor
import sys


class Simplex:
    def __init__(self, c: list = np.empty([0, 0]),
                 A: list = np.empty([0, 0]),
                 b: list = np.empty([0, 0]),
                 minmax="MAX", transform=False, printConsole=True, printFile=False):
        self.A = []
        for arr in A:
            self.A.append([Fraction(elem) for elem in arr])

        self.b = [Fraction(elem) for elem in b]
        self.c = [Fraction(elem) for elem in c]

        self.error = None

        self.minmax = minmax
        self.transform = transform
        self.printConsole = printConsole
        self.printFile = printFile
        self.file = None
        self.filename = ""

        self.transitionalX = []
        self.transitionalTableaus = []
        self.transitionalValues = []

        self.integerSubProblems = []

        self.x = [float(0)] * len(c)
        self.optimalValue = 0
        self.optimalTableau = np.empty([0, 0])

        self.intX = [float(0)] * len(c)
        self.intOptimalValue = 0
        self.intOptimalTableau = np.empty([0, 0])

    def set_A(self, A: list):
        self.A = []
        for arr in A:
            self.A.append([Fraction(elem) for elem in arr])

    def set_b(self, b: list):
        self.b = [Fraction(elem) for elem in b]

    def set_c(self, c: list):
        self.c = [Fraction(elem) for elem in c]
        self.transform = False

    def setObj(self, minmax: str):
        if minmax == "MIN" or minmax == "MAX":
            self.minmax = minmax
        else:
            raise AttributeError("Minmax attribute can hold MIN or MAX only")
        self.transform = False

    def checkFile(self):
        if not self.file:
            now = datetime.now()
            self.filename = f"simplex_{now.strftime('%d-%m-%Y_%H-%M-%S')}.txt"
            self.file = open(self.filename, 'a')

    def setPrintConsoleIter(self, printConsoleIter: bool):
        self.printConsole = printConsoleIter

    def getTableau(self):
        """
        Method for creating and filling the tableau of the LP task

        Returns:
            - NumPy array with tableau of the task
        """
        # construct starting tableau

        if self.minmax == "MIN" and self.transform is False:
            self.c[0:len(self.c)] = -1 * self.c[0:len(self.c)]
            self.transform = True

        initialTableRow = np.array([None, 0])
        numVar = len(self.c)
        numSlack = len(self.A)

        initialTableRow = np.hstack(([None], [0], [-elem for elem in self.c], [0] * numSlack))

        basis = np.array([0] * numSlack)

        for i in range(0, len(basis)):
            basis[i] = numVar + i

        A = self.A

        if not ((numSlack + numVar) == len(self.A[0])):
            B = np.identity(numSlack)
            A = np.hstack((self.A, B))

        indexedTableRows = np.hstack((np.transpose([basis]), np.transpose([self.b]), A))

        tableau = np.vstack((initialTableRow, indexedTableRows))
        tableau = np.array(tableau)
        for i in range(len(tableau)):
            for j in range(1, len(tableau[i])):
                tableau[i][j] = Fraction(tableau[i][j])
        return tableau

    def printTableau(self, tableau):
        maxLen = 3
        for row in tableau:
            for element in row:
                if len(str(element)) > maxLen:
                    maxLen = len(str(element))

        print(f"{'ind':<{maxLen}} | {'A0':<{maxLen}} | ", end="")
        for i in range(0, len(self.c)):
            print(f"A{i + 1:<{maxLen-1}}", end=" | ")
        for i in range(0, (len(tableau[0]) - len(self.c) - 2)):
            print(f"A{i + len(self.c) + 1:<{maxLen-1}}", end=" | ")
        print()

        for i in range(0, len(tableau)):
            for j in range(0, len(tableau[0])):
                if i > 0 and j == 0:
                    print(f"{'x' + str(tableau[i, j]+1):<{maxLen}}", end=" | ")
                elif tableau[i, j] is not None:
                    print(f"{str(tableau[i, j]):<{maxLen}}", end=" | ")
                else:
                    print(f"{' ' * maxLen} | ", end='')
            print()

    def printTableauFile(self, tableau):
        self.checkFile()
        maxLen = 3
        for row in tableau:
            for element in row:
                if len(str(element)) > maxLen:
                    maxLen = len(str(element))

        print(f"{'ind':<{maxLen}} | {'A0':<{maxLen}} | ", end="", file=self.file)
        for i in range(0, len(self.c)):
            print(f"A{i + 1:<{maxLen - 1}}", end=" | ", file=self.file)
        for i in range(0, (len(tableau[0]) - len(self.c) - 2)):
            print(f"A{i + len(self.c) + 1:<{maxLen - 1}}", end=" | ", file=self.file)
        print(file=self.file)

        for i in range(0, len(tableau)):
            for j in range(0, len(tableau[0])):
                if i > 0 and j == 0:
                    print(f"{'x' + str(tableau[i, j] + 1):<{maxLen}}", end=" | ", file=self.file)
                elif tableau[i, j] is not None:
                    print(f"{str(tableau[i, j]):<{maxLen}}", end=" | ", file=self.file)
                else:
                    print(f"{' ' * maxLen} | ", end='', file=self.file)
            print(file=self.file)

    def pivoting(self, tableau, r, n):
        pivot = tableau[r, n]
        if self.printConsole:
            print("Pivot Row:", r)
            print("Pivot Column:", n)
            print("Pivot Element: ", pivot)

        if self.printFile:
            self.checkFile()
            print("Pivot Row:", r, file=self.file)
            print("Pivot Column:", n, file=self.file)
            print("Pivot Element: ", pivot, file=self.file)

        tableau[r, 1:] = tableau[r, 1:] / pivot
        # pivot other rows
        for i in range(0, len(tableau)):
            if i != r:
                mult = tableau[i, n] / tableau[r, n]
                tableau[i, 1:] = tableau[i, 1:] - mult * tableau[r, 1:]
        # new basic variable
        tableau[r, 0] = n - 2
        return tableau

    def general_optimize(self):
        """Method for optimization problem and getting result"""
        tableau = self.getTableau()

        if self.printConsole:
            print("Starting Tableau:")
            self.printTableau(tableau)

        if self.printFile:
            self.checkFile()
            print("Starting Tableau:", file=self.file)
            self.printTableauFile(tableau)

        # assume initial basis is not optimal
        optimal = False
        # keep track of iterations for display
        iteration = 1
        while True:
            if self.printConsole:
                print("\n----------------------------------")
                print("Iteration :", iteration)
                self.printTableau(tableau)

            if self.printFile:
                self.checkFile()
                print("\n----------------------------------", file=self.file)
                print("Iteration :", iteration, file=self.file)
                self.printTableauFile(tableau)

            x_arr = self.getX(tableau=tableau, c=self.c)
            self.transitionalX.append(x_arr.copy())
            self.transitionalTableaus.append(tableau.copy())
            self.transitionalValues.append(tableau[0, 1])

            if self.minmax == "MAX":
                for profit in tableau[0, 2:]:
                    if profit < 0:
                        optimal = False
                        break
                    optimal = True
            else:
                for cost in tableau[0, 2:]:
                    if cost > 0:
                        optimal = False
                        break
                    optimal = True
            # if all directions result in decreased profit or increased cost
            if optimal:
                break

            # nth variable enters basis, account for tableau indexing
            if self.minmax == "MAX":
                n = tableau[0, 2:].tolist().index(np.amin(tableau[0, 2:])) + 2
            else:
                n = tableau[0, 2:].tolist().index(np.amax(tableau[0, 2:])) + 2
            # minimum ratio test, rth variable leaves basis
            minimum = sys.maxsize
            r = -1

            for i in range(1, len(tableau)):
                if tableau[i, n] > 0:
                    val = tableau[i, 1] / tableau[i, n]
                    if val < minimum:
                        minimum = val
                        r = i

            self.pivoting(tableau=tableau, r=r, n=n)

            if self.printConsole:
                print("\n----------------------------------")
                print(f"Final Tableau of {iteration} iteration")
                self.printTableau(tableau)

            if self.printFile:
                self.checkFile()
                print("\n----------------------------------", file=self.file)
                print(f"Final Tableau of {iteration} iteration", file=self.file)
                self.printTableauFile(tableau)

            iteration += 1

        # save coefficients
        self.x = self.getX(tableau=tableau, c=self.c)
        self.optimalValue = tableau[0, 1]
        self.optimalTableau = tableau

    @staticmethod
    def isXInt(xArr: list):
        isInt = True
        for elem in xArr:
            isInt = isInt and float(elem) == int(elem)
        return isInt

    @staticmethod
    def getX(tableau: np.ndarray, c: list):
        x = np.array([0] * len(c), dtype=Fraction)
        for key in range(1, len(tableau)):
            if tableau[key, 0] < len(c):
                x[int(tableau[key, 0])] = tableau[key, 1]
        return x

    def integer_optimize(self):
        try:
            if self.optimalTableau is None or self.optimalTableau == np.empty([0, 0]):
                return -2

            if Simplex.isXInt(self.x):
                self.intOptimalTableau = self.optimalTableau
                self.intX = self.x
                self.intOptimalValue = self.optimalValue
                return 1

            if self.printConsole:
                print("Starting optimizing Tableau for integers:")
                self.printTableau(self.optimalTableau)

            if self.printFile:
                self.checkFile()
                print("Starting optimizing Tableau for integers:", file=self.file)
                self.printTableauFile(self.optimalTableau)

            self.__integer_optimize_iter(self.optimalTableau)

            if not self.integerSubProblems:
                return 0

            maxValue = -sys.maxsize
            index = -1
            for i in range(len(self.integerSubProblems)):
                if self.integerSubProblems[i]["value"] > maxValue:
                    maxValue = self.integerSubProblems[i]["value"]
                    index = i

            if index > 0:
                self.intOptimalTableau = self.integerSubProblems[index]["tableau"]
                self.intX = self.integerSubProblems[index]["x"]
                self.intOptimalValue = self.integerSubProblems[index]["value"]

            if self.printConsole:
                for i in range(len(self.integerSubProblems)):
                    print(f"\n{i + 1} solution:")
                    subproblem = self.integerSubProblems[i]
                    x = subproblem["x"]
                    print(f"Integer variables:")
                    for j in range(len(x)):
                        print(f"x{j+1}: {x[j]}", end="; ")
                    print(f"\nResult: {subproblem['value']}")

                print("\nFinal integer variables:")
                for i in range(len(self.intX)):
                    print(f"x{i+1}: {self.intX[i]}", end="; ")
                print(f"\nFinal result (record): {self.intOptimalValue}")
                self.printTableau(self.optimalTableau)

            if self.printFile:
                self.checkFile()
                for i in range(len(self.integerSubProblems)):
                    print(f"\nSolution {i + 1}:", file=self.file)
                    subproblem = self.integerSubProblems[i]
                    x = subproblem["x"]
                    print(f"Integer variables:", file=self.file)
                    for j in range(len(x)):
                        print(f"x{j+1}: {x[j]}", end="; ", file=self.file)
                    print(f"\nResult: {subproblem['value']}", file=self.file)

                print("\nFinal integer results:", file=self.file)
                for i in range(len(self.intX)):
                    print(f"x{i+1}: {self.intX[i]}", end="; ", file=self.file)
                print(f"\nFinal result (record): {self.intOptimalValue}", file=self.file)
                self.printTableauFile(self.intOptimalTableau)
            return 1
        except Exception as ex:
            self.error = ex
            return -1

    def __integer_optimize_iter(self, tableau, subproblem=""):
        varsN = len(tableau[0, 2:])

        x = self.getX(tableau=tableau, c=self.c)
        minim = sys.maxsize
        xMin = -1
        for i in range(len(x)):
            if int(x[i]) != float(x[i]) and x[i] < minim:
                minim = x[i]
                xMin = i

        tableauXMinInd = 0
        for key in range(1, len(tableau)):
            if tableau[key, 0] == xMin:
                tableauXMinInd = key
                break

        left_tableau = tableau.copy()
        left_subproblem = subproblem + ("." if subproblem != "" else "") + "1"
        self.__integer_optimize_subproblem(x=x, varsN=varsN, xMin=xMin, tableauXMinInd=tableauXMinInd,
                                           tableau=left_tableau, subproblem=left_subproblem, left=True)

        right_tableu = tableau.copy()
        right_subproblem = subproblem + ("." if subproblem != "" else "") + "2"
        self.__integer_optimize_subproblem(x=x, varsN=varsN, xMin=xMin, tableauXMinInd=tableauXMinInd,
                                           tableau=right_tableu, subproblem=right_subproblem, left=False)

    def __integer_optimize_subproblem(self, x, varsN,
                                      xMin, tableauXMinInd,
                                      tableau: np.array,
                                      subproblem="", left=True):
        newCol = np.vstack([0] * len(tableau))
        tableau = np.hstack((tableau, newCol))

        tableauXMinRow = tableau[tableauXMinInd]

        if left:
            newRow = np.hstack(([varsN], [Fraction(floor(x[xMin]))], [0] * varsN, [1]))
            newRow[xMin + 2] = 1
            newRow[1:-1] = newRow[1:-1] - tableauXMinRow[1:-1]
        else:
            newRow = np.hstack(([varsN], [Fraction(-ceil(x[xMin]))], [0] * varsN, [1]))
            newRow[xMin + 2] = -1
            newRow[1:-1] = newRow[1:-1] + tableauXMinRow[1:-1]

        tableau = np.vstack((tableau, newRow))

        if self.printConsole:
            print("\n----------------------------------")
            eq = f"<={str(floor(x[xMin]))}" if left else f">={str(ceil(x[xMin]))}"
            print(f"Subproblem {subproblem}: x{xMin+1} {eq}")
            self.printTableau(tableau)

        if self.printFile:
            self.checkFile()
            print("\n----------------------------------", file=self.file)
            eq = f"<={str(floor(x[xMin]))}" if left else f">={str(ceil(x[xMin]))}"
            print(f"Subproblem {subproblem}: x{xMin+1} {eq}", file=self.file)
            self.printTableauFile(tableau)

        r = len(tableau) - 1
        minimum = sys.maxsize
        n = -1

        for j in range(2, len(tableau[0])):
            if tableau[r, j] < 0:
                val = tableau[0, j] / abs(tableau[r, j])
                if val < minimum:
                    minimum = val
                    n = j

        if n < 2:
            if self.printConsole:
                print(f"Tableau has no negative element at {r+1} row (newly added),"
                      "not possible to continue algorithm, ending branch")

            if self.printFile:
                self.checkFile()
                print(f"Tableau has no negative element at {r+1} row (newly added),"
                      "not possible to continue algorithm, ending branch", file=self.file)
            return -1
        self.pivoting(tableau=tableau, r=r, n=n)

        if self.printConsole:
            print("\n----------------------------------")
            eq = f"<={str(floor(x[xMin]))}" if left else f">={str(ceil(x[xMin]))}"
            print(f"Final Tableau of subproblem {subproblem}: x{xMin+1} {eq}")
            self.printTableau(tableau)

        if self.printFile:
            self.checkFile()
            print("\n----------------------------------", file=self.file)
            eq = f"<={str(floor(x[xMin]))}" if left else f">={str(ceil(x[xMin]))}"
            print(f"Final Tableau of subproblem {subproblem}: x{xMin+1} {eq}", file=self.file)
            self.printTableauFile(tableau)

        for i in tableau[1:, 1]:
            if i < 0:
                if self.printConsole:
                    print("Tableau has negative element(s) at 2nd column "
                          "(values of variables and function), ending branch")

                if self.printFile:
                    self.checkFile()
                    print("Tableau has negative element(s) at 2nd column "
                          "(values of variables and function), ending branch")
                return -1

        x = self.getX(tableau=tableau, c=self.c)
        if self.isXInt(x):
            self.integerSubProblems.append({"x": x, "value": tableau[0, 1], "tableau": tableau})
            if self.printConsole:
                print("Tableau has integer only elements at 2nd column "
                      "(values of variables and function), ending branch and saving tableau")

            if self.printFile:
                self.checkFile()
                print("Tableau has integer only elements at 2nd column "
                      "(values of variables and function), ending branch and saving tableau", file=self.file)
        else:
            self.__integer_optimize_iter(tableau=tableau, subproblem=subproblem)
        return 1


if __name__ == "__main__":
    my_simplex = Simplex([658, 688], [
        [1, 1],
        [1, 0],
        [5, 8],
        [6, 4],
        [4, 2]
    ], [12, 18, 89, 79, 35], printFile=True)

    my_simplex.general_optimize()
    print()

    print(my_simplex.x, my_simplex.optimalValue)
    my_simplex.printTableau(my_simplex.optimalTableau)

    my_simplex.integer_optimize()
    print(my_simplex.intX, my_simplex.intOptimalValue)
    my_simplex.printTableau(my_simplex.intOptimalTableau)

    now = datetime.now()
    print(f"simplex_{now.strftime('%d-%m-%Y_%H-%M-%S')}.txt")
