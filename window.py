from tkinter import *
from tkinter import ttk, messagebox
from linear_integer_programming import Simplex

FONT_SMALL = "Arial 10"
FONT_MEDIUM = "Arial 11"
FONT_BIG = "Arial 12"

ENTRY_WIDTH = 5
ENTRY_PAD_Y = 5
ENTRY_PAD_X = 10


def intValidate(string: str, min: int = None, max: int = None):
    value = 0
    try:
        value = int(string)
        if min is not None and value < min:
            messagebox.showerror("Помилка!", f"Введене число менше за мінімально можливе ({min}).")
            return False
        if max is not None and value > max:
            messagebox.showerror("Помилка!", f"Введене число бульше за максимально можливе ({max}).")
            return False
        return value
    except ValueError as ex:
        messagebox.showerror("Помилка!", "Введені дані не є цілим числом!")
        return False


def window():
    def calculate():
        cargoInt = intValidate(cargo.get(), 0)
        if cargoInt is False:
            return
        postInt = intValidate(post.get(), 0)
        if postInt is False:
            return
        roughInt = intValidate(rough.get(), 0)
        if roughInt is False:
            return
        compartmentInt = intValidate(compartment.get(), 0)
        if compartmentInt is False:
            return
        softInt = intValidate(soft.get(), 0)
        if softInt is False:
            return

        roughPassengersInt = intValidate(roughPassengers.get(), 0)
        if roughPassengersInt is False:
            return
        compartmentPassengersInt = intValidate(compartmentPassengers.get(), 0)
        if compartmentPassengersInt is False:
            return
        softPassengersInt = intValidate(softPassengers.get(), 0)
        if softPassengersInt is False:
            return

        fast_cargoInt = intValidate(fast_cargo.get(), 0)
        if fast_cargoInt is False:
            return
        fast_postInt = intValidate(fast_post.get(), 0)
        if fast_postInt is False:
            return
        fast_roughInt = intValidate(fast_rough.get(), 0)
        if fast_roughInt is False:
            return
        fast_compartmentInt = intValidate(fast_compartment.get(), 0)
        if fast_compartmentInt is False:
            return
        fast_softInt = intValidate(fast_soft.get(), 0)
        if fast_softInt is False:
            return

        pass_cargoInt = intValidate(pass_cargo.get(), 0)
        if pass_cargoInt is False:
            return
        pass_postInt = intValidate(pass_post.get(), 0)
        if pass_postInt is False:
            return
        pass_roughInt = intValidate(pass_rough.get(), 0)
        if pass_roughInt is False:
            return
        pass_compartmentInt = intValidate(pass_compartment.get(), 0)
        if pass_compartmentInt is False:
            return
        pass_softInt = intValidate(pass_soft.get(), 0)
        if pass_softInt is False:
            return

        c1 = roughPassengersInt * fast_roughInt
        c1 += compartmentPassengersInt * fast_compartmentInt
        c1 += softPassengersInt * fast_softInt

        c2 = roughPassengersInt * pass_roughInt
        c2 += compartmentPassengersInt * pass_compartmentInt
        c2 += softPassengersInt * pass_softInt

        c = [c1, c2]

        A = [
            [fast_cargoInt, pass_cargoInt],
            [fast_postInt, pass_postInt],
            [fast_roughInt, pass_roughInt],
            [fast_compartmentInt, pass_compartmentInt],
            [fast_softInt, pass_softInt]
        ]

        b = [cargoInt, postInt, roughInt, compartmentInt, softInt]

        simplex = Simplex(c, A, b, printConsole=False, printFile=True)
        simplex.general_optimize()
        result = simplex.integer_optimize()

        if result == 1:
            messagebox.showinfo("Успіх!", f"Результати розрахунків покроково знаходяться у файлі {simplex.filename}\n"
                                          f"Оптимальна кількість потягів:\n"
                                          f"{simplex.intX[0]} швидких, {simplex.intX[1]} пасажирських;\n"
                                          f"Оптимальна кількість пасажирів — {simplex.intOptimalValue}")
        elif result == 0:
            messagebox.showwarning("Невдача!", "Оптимальних цілочисельних розв'язків не існує.")
        elif result == -1:
            messagebox.showerror("Помилка в роботі програми!", f"Помилка: {simplex.error}, "
                                                               f"тип помилки:{simplex.error.__class__.__name__}")
        elif result == -2:
            messagebox.showerror("Відсутність даних!",
                                 "В об'єкті класу обрахунків немає оптимального нецілочисельного розв'язку для"
                                 "виконання пошуку цілочисельних.")

    def submit(event):
        calculate()

    def labelEntryInit(frame: ttk.Frame, text: str, row: int, column: int, textvariable: StringVar, columnspan=2):
        Label(frame, text=text, font=FONT_MEDIUM).grid(row=row, column=column, columnspan=columnspan, sticky=E)
        Entry(frame, textvariable=textvariable, width=ENTRY_WIDTH, font=FONT_MEDIUM) \
            .grid(row=row, column=column + columnspan, pady=ENTRY_PAD_Y, padx=ENTRY_PAD_X, sticky=W)

    win = Tk()
    win.title("TrainsCalc")
    win.wm_iconbitmap('logo.ico')
    win.resizable(False, False)
    win.bind('<Return>', submit)

    frame = ttk.Frame(win, padding="30 20")
    frame.grid(column=0, row=0, sticky=(N, W, E, S))
    win.columnconfigure(0, weight=1)
    win.rowconfigure(0, weight=1)

    cargo = StringVar()
    cargo.set(0)
    post = StringVar()
    post.set(0)
    rough = StringVar()
    rough.set(0)
    compartment = StringVar()
    compartment.set(0)
    soft = StringVar()
    soft.set(0)

    roughPassengers = StringVar()
    roughPassengers.set(0)
    compartmentPassengers = StringVar()
    compartmentPassengers.set(0)
    softPassengers = StringVar()
    softPassengers.set(0)

    fast_cargo = StringVar()
    fast_cargo.set(0)
    fast_post = StringVar()
    fast_post.set(0)
    fast_rough = StringVar()
    fast_rough.set(0)
    fast_compartment = StringVar()
    fast_compartment.set(0)
    fast_soft = StringVar()
    fast_soft.set(0)

    pass_cargo = StringVar()
    pass_cargo.set(0)
    pass_post = StringVar()
    pass_post.set(0)
    pass_rough = StringVar()
    pass_rough.set(0)
    pass_compartment = StringVar()
    pass_compartment.set(0)
    pass_soft = StringVar()
    pass_soft.set(0)

    Label(frame, text="Загальна кількість вагонів", justify=RIGHT, font=FONT_BIG) \
        .grid(column=0, row=0, columnspan=3, sticky=EW, pady=(0, 10))

    Label(frame, text="Дані про пасажирів", justify=RIGHT, font=FONT_BIG) \
        .grid(column=4, row=0, columnspan=3, sticky=EW, pady=(0, 10))

    labelEntryInit(frame, "Багажних вагонів:", 1, 0, cargo)
    labelEntryInit(frame, "Поштових вагонів:", 2, 0, post)
    labelEntryInit(frame, "Жорстких вагонів:", 3, 0, rough)
    labelEntryInit(frame, "Купейних вагонів:", 4, 0, compartment)
    labelEntryInit(frame, "М'яких вагонів:", 5, 0, soft)
    labelEntryInit(frame, "Пасажирів на вагон:", 3, 3, roughPassengers)
    labelEntryInit(frame, "Пасажирів на вагон:", 4, 3, compartmentPassengers)
    labelEntryInit(frame, "Пасажирів на вагон:", 5, 3, softPassengers)

    Label(frame, text="Швидкий потяг", justify=RIGHT, font=FONT_BIG) \
        .grid(column=0, row=6, columnspan=3, sticky=EW, pady=(20, 10))
    labelEntryInit(frame, "Багажних вагонів:", 7, 0, fast_cargo)
    labelEntryInit(frame, "Поштових вагонів:", 8, 0, fast_post)
    labelEntryInit(frame, "Жорстких вагонів:", 9, 0, fast_rough)
    labelEntryInit(frame, "Купейних вагонів:", 10, 0, fast_compartment)
    labelEntryInit(frame, "М'яких вагонів:", 11, 0, fast_soft)

    Label(frame, text="Пасажирський потяг", justify=RIGHT, font=FONT_BIG) \
        .grid(column=3, row=6, columnspan=3, sticky=EW, pady=(20, 10))
    labelEntryInit(frame, "Багажних вагонів:", 7, 3, pass_cargo)
    labelEntryInit(frame, "Поштових вагонів:", 8, 3, pass_post)
    labelEntryInit(frame, "Жорстких вагонів:", 9, 3, pass_rough)
    labelEntryInit(frame, "Купейних вагонів:", 10, 3, pass_compartment)
    labelEntryInit(frame, "М'яких вагонів:", 11, 3, pass_soft)

    button = Button(frame, text="Рахувати", font=FONT_MEDIUM, command=calculate)
    button.grid(columnspan=6, row=100, pady=(15, 0))

    win.mainloop()


if __name__ == "__main__":
    window()
